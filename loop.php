<div id="intro" class="glide">
  <div class="glide__track" data-glide-el="track">
    <ul class="glide__slides">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<li id="post-<?php the_ID(); ?>" class="glide__slide" >

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>
		<!-- /post thumbnail -->

		<!-- post title -->
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->

		

		<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

		

	</li>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>


    </ul>
	</div>
  </div>
